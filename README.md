# Automotion-Desktop-App
This is the repository for the Automotion Desktop-App.

## Usage
It is highly recommended using a prebuilt version of this program from the [releases page](https://gitlab.com/automotion/Desktop-App/-/releases) if you want to use this application.

If you still want to run the python code itself for some reason, you will need to clone this repo with `git clone --recurse-submodules https://gitlab.com/automotion/Desktop-App.git` and install the following dependencies:

- [PySide6](https://pypi.org/project/PySide6/)
- [pyqtgraph](https://pypi.org/project/pyqtgraph/)
- [numpy](https://pypi.org/project/numpy/)
- [appdirs](https://pypi.org/project/appdirs/)
- [geographiclib](https://pypi.org/project/geographiclib/)
- [PILLOW](https://pypi.org/project/Pillow/)
- [python-slugify](https://pypi.org/project/python-slugify/)
- [requests](https://pypi.org/project/requests/)
- [s2sphere](https://pypi.org/project/s2sphere/)
- [slugify](https://pypi.org/project/slugify/)
- [svgwrite](https://pypi.org/project/svgwrite/)
- [types-requests](https://pypi.org/project/types-requests/)

If you want to install them all with one command:
```sh
pip install PySide6 pyqtgraph numpy appdirs geographiclib PILLOW python-slugify requests s2sphere slugify svgwrite types-requests
```
Then you should be able to run the program using `python main.py`

### Launch arguments
- `--plot-white`&nbsp;Sets the Plot View background to white.

## Please Note
Technically, this program also depends on [py-staticmaps](https://pypi.org/project/py-staticmaps/), however, this package **must not** be installed with pip, instead the [Automotion fork of py-staticmaps](https://gitlab.com/automotion/py-staticmaps) has to be used, otherwise you will encounter issues. If you cloned this repo with `--recurse-submodules`, you are fine and won't need to worry about this as git will pull in the correct version of py-staticmaps for you.
